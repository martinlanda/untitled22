import QtQuick 2.0
import QtQuick.Controls 2.13

ApplicationWindow {
    id: mainWindow

    Background {
        id: background
        anchors.fill: parent

        Header {
            id: header
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
        }
    }


}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.2}
}
##^##*/
