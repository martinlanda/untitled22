import QtQuick 2.0
import QtQuick.Controls 2.13


Item {
    id: item1
    width: 1920
    height: 60
    Rectangle {
        id: rectangle
        color: "#262c30"
        radius: 0
        border.width: 0
        anchors.fill: parent

        Image {
            id: component3
            y: 36
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            source: "Component 3.svg"
            smooth: false
            cache: false
            sourceSize.height: 36
            sourceSize.width: 36
            anchors.leftMargin: 15
        }

        Text {
            id: text1
            y: 23
            color: "#ffffff"
            text: "EEG RECORD"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: component3.right
            font.pixelSize: 16
            anchors.leftMargin: 10
        }

        Rectangle {
            id: rectangle1
            width: 215
            height: 40
            color: "#4dffffff"
            radius: 20
            border.color: "#1affffff"
            border.width: 1
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 184
            anchors.verticalCenterOffset: 0
        }

        Image {
            id: windowBtn
            x: 1540
            y: 20
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            source: "windowBtn.svg"
            anchors.rightMargin: 10
            fillMode: Image.PreserveAspectFit
        }
    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.25}
}
##^##*/
